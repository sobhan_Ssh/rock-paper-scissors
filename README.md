# Rock-Paper-Scissors

The goal was to develop a very simple project in order to support functionalities of a **ROCK-PAPER-SCISSORS** game.
<br/>

<br/>
This project implemented with the following stack:
<ul>
<li>JAVA, as the base language of project</li>
<li>MAVEN, as the build tool and dependency manager for the project</li>
<li>JUNIT, is used in order to write unit tests to check the games functionalities</li>
</ul>

# HOW TO RUN PROJECT
**You just need to have `Maven` installed on your machine, then follow the steps below**

1. Clone the repository, in `/wallet-service` directory using command :
`git clone https://gitlab.com/sobhan_Ssh/rock-paper-scissors.git`

2. `cd rock-paper-scissors` (the directory that contains the pom.xml)

3. Build the project using maven command :
   `mvn clean install`,
This will compile your project and create the jar you defined in the pom.xml file. It runs the maven phases clean and every phase up to install (compile, test, etc).

4. Then collect all jar files you use as dependencies (required to run your project):
`mvn dependency:copy-dependencies`

5. You can then run your main method using:

`cd target/`

`java -cp rock-paper-scissors-1.0-SNAPSHOT.jar:dependency Main`
