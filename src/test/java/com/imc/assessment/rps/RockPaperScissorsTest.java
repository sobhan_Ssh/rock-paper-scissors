package com.imc.assessment.rps;

import com.imc.assessment.rps.enums.Action;
import com.imc.assessment.rps.enums.Result;
import com.imc.assessment.rps.game.Match;
import com.imc.assessment.rps.game.RockPaperScissors;
import com.imc.assessment.rps.player.ComputerPlayer;
import com.imc.assessment.rps.player.Player;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class RockPaperScissorsTest {

    @Test
    public void test_player_is_winner_when_player_is_rock_and_computer_is_scissors() {
        RockPaperScissors game = initOneRoundGameInstanceWithPreDefinedActions(1, Action.SCISSORS, Action.PAPER);
        Assert.assertEquals(game.getPlayerScore(), 1);
        Assert.assertEquals(game.getComputerScore(), 0);
        Assert.assertEquals(game.getGameResult(), Result.PLAYER_WON);
    }

    @Test
    public void test_computer_is_winner_when_player_is_rock_and_computer_is_paper() {
        RockPaperScissors game = initOneRoundGameInstanceWithPreDefinedActions(1, Action.ROCK, Action.PAPER);
        Assert.assertEquals(game.getComputerScore(), 1);
        Assert.assertEquals(game.getPlayerScore(), 0);
        Assert.assertEquals(game.getGameResult(), Result.COMPUTER_WON);
    }

    @Test
    public void test_result_is_tied_when_player_and_computer_have_same_choice() {
        RockPaperScissors game = initOneRoundGameInstanceWithPreDefinedActions(1, Action.PAPER, Action.PAPER);
        Assert.assertEquals(game.getComputerScore(), 0);
        Assert.assertEquals(game.getPlayerScore(), 0);
        Assert.assertEquals(game.getGameResult(), Result.TIED);
    }

    @Test
    public void test_match_result_is_computerWon_when_player_is_rock_and_computer_is_paper() {
        Player player = new Player("Sobhan", 0);
        ComputerPlayer computerPlayer = new ComputerPlayer("pc", 0);
        Match match = new Match(player, computerPlayer, Action.ROCK, Action.PAPER);
        Result result = match.calculateMatchResult();
        Assert.assertEquals(result, Result.COMPUTER_WON);
    }

    @Test
    public void test_match_result_is_playerWon_when_player_is_rock_and_computer_is_scissors() {
        Player player = new Player("Sobhan", 0);
        ComputerPlayer computerPlayer = new ComputerPlayer("pc", 0);
        Match match = new Match(player, computerPlayer, Action.ROCK, Action.SCISSORS);
        Result result = match.calculateMatchResult();
        Assert.assertEquals(result, Result.PLAYER_WON);
    }

    @Test
    public void test_match_result_is_tied_when_player_is_scissors_and_computer_is_scissors() {
        Player player = new Player("Sobhan", 0);
        ComputerPlayer computerPlayer = new ComputerPlayer("pc", 0);
        Match match = new Match(player, computerPlayer, Action.SCISSORS, Action.SCISSORS);
        Result result = match.calculateMatchResult();
        Assert.assertEquals(result, Result.TIED);
    }

    private RockPaperScissors initOneRoundGameInstanceWithPreDefinedActions(int roundCount, Action playerAction, Action computerAction) {
        RockPaperScissors game = new RockPaperScissors();
        Player player = new Player("Sobhan", 0);
        ComputerPlayer computerPlayer = new ComputerPlayer("pc", 0);
        List<Match> matchList = new ArrayList<>();
        for (int i = 0; i < roundCount; i++) {
            Match match = new Match(player, computerPlayer, playerAction, computerAction);
            matchList.add(match);
        }
        game.setMatches(matchList);
        game.calculateMatchesResult();
        return game;
    }

    private RockPaperScissors initOneRoundGameInstanceWithRandomActions(int roundCount) {
        RockPaperScissors game = new RockPaperScissors();
        Player player = new Player("Sobhan", 0);
        ComputerPlayer computerPlayer = new ComputerPlayer("pc", 0);
        List<Match> matchList = new ArrayList<>();
        for (int i = 0; i < roundCount; i++) {
            Match match = new Match(player, computerPlayer, getRandomAction(), getRandomAction());
            matchList.add(match);
        }
        game.setMatches(matchList);
        game.calculateMatchesResult();
        return game;
    }

    private Action getRandomAction() {
        List<Action> VALUES = Collections.unmodifiableList(Arrays.asList(Action.values()));
        int SIZE = VALUES.size();
        Random RANDOM = new Random();
        return VALUES.get(RANDOM.nextInt(SIZE));
    }

}
