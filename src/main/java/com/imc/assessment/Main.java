package com.imc.assessment;

import com.imc.assessment.rps.game.RockPaperScissors;

public class Main {

    public static void main(String[] args) {
       boolean flag = true;
       while (flag) {
           RockPaperScissors game = new RockPaperScissors();
           game.startGame();
           flag = game.askForAnotherGame();
       }
    }
}