package com.imc.assessment.rps.game;

import com.imc.assessment.rps.enums.Action;
import com.imc.assessment.rps.enums.Result;
import com.imc.assessment.rps.player.ComputerPlayer;
import com.imc.assessment.rps.player.Player;

public class Match {

    private final Player player;
    private final ComputerPlayer computerPlayer;
    private final Action playerAction;
    private final Action computerAction;
    private Result result;
    private String resultMsg;

    public Match(Player player, ComputerPlayer computerPlayer, Action playerAction, Action computerAction) {
        this.player = player;
        this.computerPlayer = computerPlayer;
        this.playerAction = playerAction;
        this.computerAction = computerAction;
    }

    public Result calculateMatchResult() {
        switch (playerAction) {
            case ROCK:
                switch (computerAction) {
                    case ROCK:
                        this.result = Result.TIED;
                        break;
                    case PAPER:
                        this.result = Result.COMPUTER_WON;
                        break;
                    case SCISSORS:
                        this.result = Result.PLAYER_WON;
                        break;
                }
                break;
            case PAPER:
                switch (computerAction) {
                    case ROCK:
                        this.result = Result.PLAYER_WON;
                        break;
                    case PAPER:
                        this.result = Result.TIED;
                        break;
                    case SCISSORS:
                        this.result = Result.COMPUTER_WON;
                        break;
                }
                break;
            case SCISSORS:
                switch (computerAction) {
                    case ROCK:
                        this.result = Result.COMPUTER_WON;
                        break;
                    case PAPER:
                        this.result = Result.PLAYER_WON;
                        break;
                    case SCISSORS:
                        this.result = Result.TIED;
                        break;
                }
                break;
        }
        return result;
    }

    public void fillResultMessage() {
        switch (result) {
            case TIED:
                this.resultMsg = "TIED!! " + player.getName() + " and " + computerPlayer.getName() + " both chose " + playerAction.name();
                break;
            case PLAYER_WON:
                this.resultMsg = "You chose " + playerAction.name() + " and " + computerPlayer.getName() + " chose " + computerAction.name() + " so " + player.getName() + " won the match";
                break;
            case COMPUTER_WON:
                this.resultMsg = "You chose " + playerAction.name() + " and " + computerPlayer.getName() + " chose " + computerAction.name() + " so " + computerPlayer.getName() + " won the match";
                break;
        }
    }

    public Player getPlayer() {
        return player;
    }

    public ComputerPlayer getComputerPlayer() {
        return computerPlayer;
    }

    public Action getPlayerAction() {
        return playerAction;
    }

    public Action getComputerAction() {
        return computerAction;
    }

    public Result getResult() {
        return result;
    }

    public String getResultMsg() {
        return resultMsg;
    }
}
