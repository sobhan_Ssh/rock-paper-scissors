package com.imc.assessment.rps.game;

import com.imc.assessment.rps.enums.Action;
import com.imc.assessment.rps.enums.Result;
import com.imc.assessment.rps.player.ComputerPlayer;
import com.imc.assessment.rps.player.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class RockPaperScissors {

    Scanner scanner = new Scanner(System.in);
    private final Player player = new Player();
    private final ComputerPlayer computerPlayer = new ComputerPlayer();
    private int roundCount;
    private Result result;
    private int playerScore;
    private int computerScore;
    private List<Match> matches = new ArrayList<>();

    private void printWelcomeMessage() {
        System.out.println("=======Rock====Paper====Scissors=======");
        System.out.println("Welcome to Rock-Paper-Scissors game");
        System.out.println("=======Rock====Paper====Scissors=======");
    }

    private void initGame() {
        player.initPlayer();
        computerPlayer.initPlayer();
        try {
            askRoundCount();
        } catch (NumberFormatException e) {
            askRoundCount();
        }
    }

    public void startGame() {
        printWelcomeMessage();
        initGame();
        runGame();
        calculateMatchesResult();
        printFinalGameResult();
    }

    private void runGame() {
        while (roundCount > 0) {
            Action playerAction, computerAction;
            computerAction = computerPlayer.getPlayerAction();
            playerAction = player.getPlayerAction();
            Match match = new Match(player, computerPlayer, playerAction, computerAction);
            matches.add(match);
            roundCount--;
        }
    }

    public void calculateMatchesResult() {
        for (int i = 0; i < matches.size(); i++) {
            Result matchResult = matches.get(i).calculateMatchResult();
            matches.get(i).fillResultMessage();
            int matchNum = i + 1;
            System.out.println("match number " + matchNum + " : ");
            System.out.println(matches.get(i).getResultMsg());
            switch (matchResult) {
                case COMPUTER_WON:
                    this.computerScore++;
                    break;
                case PLAYER_WON:
                    this.playerScore++;
                    break;
                case TIED:
                    break;
            }
        }
    }

    private void printFinalGameResult() {
        System.out.println("=======FINAL RESULT=======");
        System.out.println(player.getName() + " scored : " + playerScore);
        System.out.println(computerPlayer.getName() + " scored : " + computerScore);
        Result gameResult = getGameResult();
        if (Objects.equals(gameResult, Result.PLAYER_WON))
            System.out.println(player.getName() + " WON THE GAME!!");
        else if(Objects.equals(gameResult, Result.COMPUTER_WON))
            System.out.println(computerPlayer.getName() + " WON THE GAME!!");
        else
            System.out.println("GAME TIED!!");

    }

    private void askRoundCount() throws NumberFormatException {
        System.out.println("How many round would you play?");
        String roundStr = scanner.nextLine();
        try {
            roundCount = Integer.parseInt(roundStr);
        } catch (NumberFormatException e) {
            System.out.println("invalid input format, please enter number");
            askRoundCount();
        }
    }

    public Result getGameResult() {
        if (playerScore > computerScore)
            this.result = Result.PLAYER_WON;
        else if(computerScore > playerScore)
            this.result = Result.COMPUTER_WON;
        else
            this.result = Result.TIED;
        return result;
    }

    public boolean askForAnotherGame() {
        System.out.println("Do you want to play one more game?");
        System.out.println("To play again, type 1.\nTo exit game, type 2.");
        String playerInputStr = scanner.nextLine();
        try {
            int replay = Integer.parseInt(playerInputStr);
            if (replay == 1) {
                System.out.println("WooHoo, let's play again!!");
                return true;
            } else if (replay == 2) {
                System.out.println("OK, maybe next time!!");
                return false;
            } else {
                System.out.println("invalid input number, please enter 1 or 2");
                return askForAnotherGame();
            }
        } catch (NumberFormatException e) {
            System.out.println("invalid input format, please enter number");
            return askForAnotherGame();
        }
    }

    public Player getPlayer() {
        return player;
    }

    public ComputerPlayer getComputerPlayer() {
        return computerPlayer;
    }

    public int getRoundCount() {
        return roundCount;
    }

    public void setRoundCount(int roundCount) {
        this.roundCount = roundCount;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public int getPlayerScore() {
        return playerScore;
    }

    public void setPlayerScore(int playerScore) {
        this.playerScore = playerScore;
    }

    public int getComputerScore() {
        return computerScore;
    }

    public void setComputerScore(int computerScore) {
        this.computerScore = computerScore;
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }
}
