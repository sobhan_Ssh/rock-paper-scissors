package com.imc.assessment.rps.exception;

public class EnumParseException extends RuntimeException {

    public EnumParseException(String message) {
        super(message);
    }
}
