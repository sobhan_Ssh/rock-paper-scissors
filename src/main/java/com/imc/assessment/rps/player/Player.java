package com.imc.assessment.rps.player;

import com.imc.assessment.rps.enums.Action;
import com.imc.assessment.rps.exception.EnumParseException;

import java.util.Scanner;

public class Player implements IPlayer {

    Scanner scanner = new Scanner(System.in);
    private String name;
    private int score;

    public Player() {
    }

    public Player(String name, int score) {
        this.name = name;
        this.score = score;
    }

    @Override
    public void initPlayer() {
        System.out.println("please type your name: ");
        this.name = scanner.nextLine();
        this.score = 0;
    }

    @Override
    public Action getPlayerAction() {
        System.out.println("it's your turn");
        System.out.println("To play ROCK, type 1.\nTo play PAPER, type 2.\nTo play SCISSORS, type 3.");
        String playerInputStr = scanner.nextLine();
        Action playerAction;
        try {
            playerAction = Action.parseActionCode(Integer.parseInt(playerInputStr));
        } catch (NumberFormatException | EnumParseException e) {
            System.out.println("invalid input, please enter number 1,2 or 3");
            return getPlayerAction();
        }
        return playerAction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getScore() {
        return score;
    }
}
