package com.imc.assessment.rps.player;

import com.imc.assessment.rps.enums.Action;

public class ComputerPlayer implements IPlayer {

    private String name;
    private int score;

    @Override
    public void initPlayer() {
        this.name = "computer";
        this.score = 0;
    }

    @Override
    public Action getPlayerAction() {
        int randomNum = getRandomNumber();
        return Action.parseActionCode(randomNum);
    }

    private int getRandomNumber() {
        return (int) ((Math.random() * (4 - 1)) + 1);
    }

    public ComputerPlayer(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public ComputerPlayer() {
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
