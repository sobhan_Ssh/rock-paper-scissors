package com.imc.assessment.rps.player;

import com.imc.assessment.rps.enums.Action;

public interface IPlayer {

    void initPlayer();

    Action getPlayerAction();


}
