package com.imc.assessment.rps.enums;

import com.imc.assessment.rps.exception.EnumParseException;

public enum Action {

    ROCK(1),
    PAPER(2),
    SCISSORS(3);

    private final int code;

    Action(int code) {
        this.code = code;
    }

    public static Action parseActionCode(int code) {
        switch (code) {
            case 1:
                return ROCK;
            case 2:
                return PAPER;
            case 3:
                return SCISSORS;
            default:
                throw new EnumParseException("invalid action");
        }
    }

    public static Action parseActionString(String str) {
        switch (str.toUpperCase()) {
            case "ROCK":
                return ROCK;
            case "PAPER":
                return PAPER;
            case "SCISSORS":
                return SCISSORS;
            default:
                throw new EnumParseException("invalid action");
        }
    }
}
