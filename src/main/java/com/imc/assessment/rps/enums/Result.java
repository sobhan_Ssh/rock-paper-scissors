package com.imc.assessment.rps.enums;

import com.imc.assessment.rps.exception.EnumParseException;

public enum Result {

    TIED(1),
    COMPUTER_WON(2),
    PLAYER_WON(3);

    private final int code;

    Result(int code) {
        this.code = code;
    }

    public Result parseMatchResultCode(int code) {
        switch (code) {
            case 1:
                return TIED;
            case 2:
                return COMPUTER_WON;
            case 3:
                return PLAYER_WON;
            default:
                throw new EnumParseException("invalid match result");
        }
    }

    public Result parseMatchResultString(String str) {
        switch (str.toUpperCase()) {
            case "TIED":
                return TIED;
            case "COMPUTER_WON":
                return COMPUTER_WON;
            case "PLAYER_WON":
                return PLAYER_WON;
            default:
                throw new EnumParseException("invalid match result");
        }
    }
}
